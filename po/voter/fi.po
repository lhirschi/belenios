#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-25 10:18+0000\n"
"PO-Revision-Date: 2021-04-18 00:26+0000\n"
"Last-Translator: Janne Peltola <jannepeltola@iki.fi>\n"
"Language-Team: Finnish <https://hosted.weblate.org/projects/belenios/voter/"
"fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.6-dev\n"

#: src/web/lib/pages_common.ml:381
msgid " (if you forgot it, for example)."
msgstr " (jos esimerkiksi unohdit sen)."

#: src/web/lib/pages_voter.ml:865
msgid " A confirmation e-mail has been sent to you."
msgstr " Sinulle on lähetetty vahvistusviesti."

#: src/web/lib/pages_voter.ml:639
msgid " and "
msgstr " ja "

#: src/web/lib/pages_voter.ml:864
msgid " anytime during the election."
msgstr " milloin vain äänestyksen aikana."

#: src/web/lib/pages_voter.ml:929
msgid " ballot(s) have been accepted so far."
msgstr " äänestyslippua saatu tähän mennessä."

#: src/web/lib/pages_voter.ml:939
msgid " ballot(s) have been accepted, and "
msgstr " äänestyslippua on vastaanotettu ja "

#: src/web/lib/pages_voter.ml:934
msgid " ballot(s) have been accepted."
msgstr " äänestyslippua on vastaanotettu."

#: src/web/lib/web_common.ml:95
msgid " day(s)"
msgstr " päivä(ä)"

#: src/web/lib/pages_voter.ml:160
msgid " for more information."
msgstr " saadaksesi lisätietoa."

#: src/web/lib/pages_voter.ml:854
msgid " has been accepted."
msgstr " on vastaanotettu."

#: src/web/lib/pages_voter.ml:733
msgid " has been received, but not recorded yet. "
msgstr " on vastaanotettu, muttei vielä tallennettu. "

#: src/web/lib/pages_voter.ml:941
msgid " have been tallied."
msgstr " on laskettu."

#: src/web/lib/web_common.ml:97
msgid " hour(s)"
msgstr " tunti(a)"

#: src/web/lib/pages_common.ml:513
msgid " in the following box: "
msgstr ""

#: src/web/lib/pages_voter.ml:868
msgid " is rejected, because "
msgstr " ei hyväksytty, koska "

#: src/web/lib/web_common.ml:98
msgid " minute(s)"
msgstr " minuutti(a)"

#: src/web/lib/web_common.ml:94
msgid " month(s)"
msgstr " kuukautta"

#: src/web/lib/web_common.ml:99
msgid " second(s)"
msgstr " sekunti(a)"

#: src/web/lib/web_common.ml:93
msgid " year(s)"
msgstr " vuotta"

#: src/web/lib/pages_voter.ml:992
msgid "!"
msgstr "!"

#: src/web/lib/pages_voter.ml:129 src/web/lib/pages_voter.ml:103
msgid "%d blank ballot(s)"
msgstr ""

#: src/web/lib/pages_voter.ml:1229 src/web/lib/pages_voter.ml:107
msgid "%d invalid ballot(s)"
msgstr "%d hylättyä äänestyslippua"

#: src/web/lib/pages_voter.ml:485
msgid "%d of the following %d trustees (verification keys) [public keys] are needed to decrypt the election result:"
msgstr ""
"%d/%d seuraavista luottamushenkilöistä (vahvistusavaimet) [julkiset avaimet] "
"vaaditaan äänestystuloksen purkamiseksi:"

#: src/web/lib/pages_voter.ml:123 src/web/lib/pages_voter.ml:97
msgid "%d valid (non-blank) ballot(s)"
msgstr ""

#: src/web/lib/pages_voter.ml:124 src/web/lib/pages_voter.ml:98
msgid "%d valid ballot(s)"
msgstr ""

#: src/tool/js/tool_js_booth.ml:168
msgid "(nothing)"
msgstr "(ei mitään)"

#: src/web/lib/pages_common.ml:379
msgid ", or "
msgstr ", tai "

#: src/web/lib/pages_voter.ml:1133
msgid "1 is the highest grade, 2 is the second highest grade, etc."
msgstr "1 on korkein järjestysnumero, 2 toiseksi ylin jne."

#: src/web/lib/pages_voter.ml:154
msgid "A Condorcet winner is a candidate that is preferred over all the other candidates."
msgstr "Condorcet-voittaja on ehdokas, jota suositaan yli kaikkien muiden."

#: src/web/lib/pages_voter.ml:1235
msgid "A ballot is invalid if two candidates have been given the same preference order or if a rank is missing."
msgstr ""
"Äänestyslipuke hylätään, jos kahdelle ehdokkaalle on annettu sama "
"järjestysnumero tai jokin järjestysnumero puuttuu."

#: src/web/lib/pages_voter.ml:402
msgid "Accept"
msgstr "Hyväksy"

#: src/web/lib/pages_voter.ml:904
msgid "Accepted ballots"
msgstr "Hyväksytyt äänestyslipukkeet"

#: src/web/lib/pages_voter.ml:111
msgid "According to Majority Judgment, the ranking is:"
msgstr ""

#: src/web/lib/pages_common.ml:150 src/web/lib/pages_common.ml:80
msgid "Administer elections"
msgstr "Ylläpidä äänestyksiä"

#: src/web/lib/pages_common.ml:152 src/web/lib/pages_common.ml:82
msgid "Administer this election"
msgstr "Ylläpidä tätä äänestystä"

#: src/web/lib/pages_voter.ml:331
msgid "Advanced mode"
msgstr "Edistyneet työkalut"

#: src/web/lib/pages_voter.ml:461
msgid "All of the following trustees (verification keys) are needed to decrypt the result:"
msgstr ""
"Seuraavat luottamushenkilöt (varmistusavaimet) vaaditaan tuloksen "
"purkamiseksi:"

#: src/web/lib/pages_voter.ml:1017 src/web/lib/pages_voter.ml:810
#. src/web/lib/pages_voter.ml:808
#. src/web/lib/pages_voter.ml:676
#. src/web/lib/pages_voter.ml:674
msgid "Answer to questions"
msgstr "Vastaus kysymyksiin"

#: src/web/lib/pages_voter.ml:1135
msgid "As a convenience, 0 is always interpreted as the lowest grade."
msgstr "Helpotukseksi 0 tulkitaan aina alimmaksi vastaukseksi."

#: src/tool/js/tool_js_booth.ml:255
msgid "At least one of the answers is invalid!"
msgstr "Ainakin yksi vastaus ei kelpaa!"

#: src/web/lib/pages_voter.ml:55
msgid "Audit data: "
msgstr "Kirjausketju (audit trail): "

#: src/web/lib/pages_voter.ml:1021 src/web/lib/pages_voter.ml:828
#. src/web/lib/pages_voter.ml:826
#. src/web/lib/pages_voter.ml:694
#. src/web/lib/pages_voter.ml:692
#. src/web/lib/pages_common.ml:454
#. src/web/lib/pages_common.ml:408
#. src/web/lib/pages_common.ml:359
msgid "Authenticate"
msgstr "Tunnistus"

#: src/web/lib/pages_common.ml:317
msgid "Authenticate with %s"
msgstr ""

#: src/web/lib/pages_common.ml:419
msgid "Authentication failed"
msgstr "Tunnistus epäonnistui"

#: src/web/lib/pages_common.ml:422
msgid "Authentication failed, probably because of a bad username or password, or you are not allowed to perform this operation."
msgstr ""
"Tunnistus epäonnistui. Tämä johtui todennäköisesti väärästä "
"käyttäjätunnuksesta tai salasanasta; sinulla ei välttämättä ole oikeuksia "
"käyttää tätä toimintoa."

#: src/web/lib/pages_voter.ml:222
msgid "Available methods on this server:"
msgstr "Käytettävissä olevat menetelmät:"

#: src/web/lib/pages_common.ml:492
msgid "Bad e-mail address!"
msgstr ""

#: src/web/lib/pages_common.ml:491
msgid "Bad security code!"
msgstr ""

#: src/web/lib/pages_voter.ml:966 src/web/lib/pages_voter.ml:954
msgid "Belenios Booth"
msgstr "Belenios-koppi"

#: src/web/lib/pages_common.ml:476
msgid "Belenios Server"
msgstr ""

#: src/web/lib/pages_common.ml:478
msgid "Belenios authentication"
msgstr ""

#: src/web/lib/pages_common.ml:472
msgid "Best regards,"
msgstr ""

#: src/tool/js/tool_js_booth.ml:160 src/tool/js/tool_js_booth.ml:86
#. src/web/lib/pages_voter.ml:178
msgid "Blank vote"
msgstr "Tyhjä ääni"

#: src/web/lib/pages_voter.ml:399
msgid "By using this site, you accept our "
msgstr "Käyttämällä sivustoa hyväksyt "

#: src/tool/js/tool_js_fingerprint.ml:84
msgid "Compare"
msgstr "Vertaa"

#: src/tool/js/tool_js_fingerprint.ml:68
msgid "Compute fingerprint"
msgstr "Laske sormenjälki"

#: src/tool/js/tool_js_fingerprint.ml:35
msgid "Computed fingerprint:"
msgstr "Laskettu sormenjälki:"

#: src/web/lib/pages_voter.ml:1108
msgid "Condorcet-Schulze method"
msgstr "Condorcet-Schulzen menetelmä"

#: src/web/lib/pages_voter.ml:1023 src/web/lib/pages_voter.ml:837
#. src/web/lib/pages_voter.ml:835
#. src/web/lib/pages_voter.ml:703
#. src/web/lib/pages_voter.ml:701
msgid "Confirm"
msgstr "Vahvista"

#: src/web/lib/pages_voter.ml:1181 src/web/lib/pages_voter.ml:1124
#. src/web/lib/pages_voter.ml:1003
msgid "Continue"
msgstr "Jatka"

#: src/web/lib/site_voter.ml:115
msgid "Cookies are blocked"
msgstr "Evästeet on estetty"

#: src/web/lib/pages_voter.ml:1360
msgid "Credential:"
msgstr "Tunniste:"

#: src/web/lib/pages_voter.ml:495
msgid "Credentials were generated and sent by %s and have fingerprint %s."
msgstr "Tunnisteet on luotu, ne lähetti %s sormenjäljellä %s."

#: src/web/lib/pages_voter.ml:1396 src/web/lib/pages_common.ml:463
msgid "Dear %s,"
msgstr "Hyvä %s,"

#: src/web/lib/pages_voter.ml:1025
msgid "Done"
msgstr "Valmis"

#: src/web/lib/pages_common.ml:504 src/web/lib/pages_common.ml:388
#. src/web/lib/pages_common.ml:343
msgid "E-mail address:"
msgstr ""

#: src/web/lib/pages_voter.ml:1085
msgid "Election UUID: "
msgstr "Äänestyksen UUID: "

#: src/web/lib/pages_voter.ml:1089 src/web/lib/pages_voter.ml:51
msgid "Election fingerprint: "
msgstr "Äänestyksen sormenjälki: "

#: src/web/lib/pages_voter.ml:1068 src/web/lib/pages_common.ml:178
#. src/web/lib/pages_common.ml:110
msgid "Election server"
msgstr "Äänestyspalvelin"

#: src/web/lib/pages_voter.ml:1055
msgid "Encrypting…"
msgstr "Salataan…"

#: src/web/lib/site_voter.ml:354 src/web/lib/site_voter.ml:329
#. src/web/lib/site_voter.ml:281
#. src/web/lib/site_voter.ml:276
#. src/web/lib/site_voter.ml:271
msgid "Error"
msgstr "Virhe"

#: src/tool/js/tool_js_fingerprint.ml:75
msgid "Expected fingerprint:"
msgstr "Oletettu sormenjälki:"

#: src/web/lib/pages_voter.ml:871
msgid "FAIL!"
msgstr "VIRHE!"

#: src/web/lib/site_common.ml:110
msgid "Forbidden"
msgstr ""

#: src/web/lib/pages_common.ml:204 src/web/lib/pages_common.ml:133
msgid "Get the source code"
msgstr "Hae lähdekoodi"

#: src/web/lib/pages_voter.ml:922 src/web/lib/pages_voter.ml:885
#. src/web/lib/pages_voter.ml:782
#. src/web/lib/pages_voter.ml:752
msgid "Go back to election"
msgstr "Palaa äänestykseen"

#: src/web/lib/pages_voter.ml:637
msgid "I am "
msgstr "Olen "

#: src/web/lib/pages_voter.ml:642
msgid "I cast my vote"
msgstr "Äänestän"

#: src/tool/js/tool_js_booth.ml:233
msgid "If you are asked to grade candidates (majority judgement) then 1 is the best grade, higher numbers are worse."
msgstr ""
"Jos sinua pyydetään järjestämään ehdokkaita (enemmistövaalitapa), 1 on paras "
"järjestysluku ja korkeammat huonompia."

#: src/tool/js/tool_js_booth.ml:236
msgid "If you are asked to rank candidates (Condorcet, STV, ...) then use 1 for your first choice, 2 for the second, etc."
msgstr ""
"Jos sinua pyydetään järjestämään ehdokkaita (Condorcet, STV, ...) käytä "
"numeroa 1 ensimmäiseen valintaasi, numeroa 2 toiseen jne."

#: src/web/lib/pages_voter.ml:776
msgid "If you want to vote, you must "
msgstr "Jos haluat äänestää, sinun täytyy "

#: src/web/lib/pages_voter.ml:1259
msgid "In our implementation, when several candidates have the same number of votes when they are ready to be elected or eliminated, we follow the order in which candidates were listed in the election."
msgstr ""
"Toteutustavassamme saman verran ääniä saaneet ehdokkaat valitaan tai "
"pudotetaan alkuperäisten vaalilistojen mukaisessa järjestyksessä."

#: src/web/lib/pages_voter.ml:1131
msgid "In the context of Majority Judgment, a vote gives a grade to each candidate."
msgstr ""
"Enemmistövaalitavassa jokainen vaalilippu antaa järjestysnumeron kullekin "
"ehdokkaalle."

#: src/web/lib/pages_voter.ml:1188
msgid "In the context of STV, voters rank candidates by order of preference."
msgstr ""
"STV-vaalitavassa äänestäjät järjestävät ehdokkaat paremmuusjärjestykseen."

#: src/web/lib/pages_voter.ml:1015 src/web/lib/pages_voter.ml:801
#. src/web/lib/pages_voter.ml:799
#. src/web/lib/pages_voter.ml:667
#. src/web/lib/pages_voter.ml:665
msgid "Input credential"
msgstr "Syötä tunniste"

#: src/web/lib/pages_voter.ml:1034
msgid "Input your credential "
msgstr "Syötä tunnisteesi "

#: src/tool/js/tool_js_booth.ml:385
msgid "Invalid credential!"
msgstr "Virheellinen tunniste!"

#: src/web/lib/site_voter.ml:282
msgid "Invalid index for question."
msgstr "Virheellinen järjestysluku kysymykselle."

#: src/web/lib/pages_voter.ml:240
msgid "It contains all submitted ballots in clear, in random order."
msgstr ""

#: src/web/lib/pages_voter.ml:220
msgid "It is up to you to apply your favorite counting method."
msgstr ""

#: src/web/lib/pages_voter.ml:263
msgid "It will open in "
msgstr "Se aukeaa "

#: src/web/lib/pages_voter.ml:238
msgid "JSON result"
msgstr "JSON-tulos"

#: src/web/lib/pages_common.ml:226
msgid "Language:"
msgstr "Kieli:"

#: src/web/lib/pages_voter.ml:961
msgid "Loading…"
msgstr "Ladataan…"

#: src/web/lib/pages_common.ml:453 src/web/lib/pages_common.ml:407
#. src/web/lib/pages_common.ml:358
#. src/web/lib/pages_common.ml:335
msgid "Log in"
msgstr "Kirjaudu"

#: src/web/lib/pages_common.ml:316
msgid "Log in with %s"
msgstr "Kirjaudu %s:lla"

#: src/web/lib/pages_voter.ml:1263
msgid "Look at the raw events for more details."
msgstr "Raaka-aineistosta voi katsoa lisätietoja."

#: src/web/lib/pages_voter.ml:226
msgid "Majority Judgment"
msgstr "Enemmistövaalitapa"

#: src/web/lib/pages_voter.ml:1165 src/web/lib/pages_voter.ml:1115
msgid "Majority Judgment method"
msgstr "Enemmistövaalitapa"

#: src/web/lib/pages_voter.ml:1257
msgid "Many variants of STV exist, depending for example on how to break ties."
msgstr ""
"STV-vaalitavasta on monia muunnelmia, esimerkiksi liittyen tasatilanteiden "
"ratkaisemiseen."

#: src/web/lib/pages_voter.ml:1195
msgid "Many variants of STV exist, we documented our choices in "
msgstr ""
"STV-vaalitavasta on useita muunnelmia. Meidän valintamme on dokumentoitu "

#: src/web/lib/pages_voter.ml:1192 src/web/lib/pages_voter.ml:1139
msgid "More information can be found "
msgstr "Lisätietoa voi löytää "

#: src/tool/js/tool_js_booth.ml:317
msgid "Next"
msgstr "Seuraava"

#: src/tool/js/tool_js_booth.ml:140
msgid "No other choices are allowed when voting blank"
msgstr "Jos äänestät tyhjää, et voi valita muita vaihtoehtoja"

#: src/web/lib/site_common.ml:52
msgid "Not found"
msgstr "Ei löytynyt"

#: src/web/lib/pages_voter.ml:1302
msgid "Note that you also need a credential, sent in a separate email, to start voting."
msgstr ""

#: src/web/lib/pages_voter.ml:657
msgid "Note: You have already voted. Your vote will be replaced."
msgstr "Huoio: Olet jo äänestänyt. Aiempi vaalilippusi korvataan."

#: src/web/lib/pages_voter.ml:743
msgid "Note: your ballot is encrypted and nobody can see its contents."
msgstr "Huomaa: vaalilippusi on salattu ja kukaan ei voi nähdä sen sisältöä."

#: src/tool/js/tool_js_booth.ml:228
msgid "Notes:"
msgstr "Huomiot:"

#: src/web/lib/pages_voter.ml:367
msgid "Number of accepted ballots: "
msgstr ""

#: src/web/lib/pages_voter.ml:1121
msgid "Number of grades:"
msgstr ""

#: src/web/lib/pages_voter.ml:1178
msgid "Number of seats:"
msgstr ""

#: src/web/lib/pages_voter.ml:1365 src/web/lib/pages_voter.ml:1310
msgid "Number of votes:"
msgstr ""

#: src/web/lib/pages_voter.ml:1371 src/web/lib/pages_voter.ml:1316
msgid "Only the last vote counts."
msgstr ""

#: src/web/lib/pages_voter.ml:1368 src/web/lib/pages_voter.ml:1313
msgid "Page of the election:"
msgstr ""

#: src/web/lib/pages_voter.ml:1306 src/web/lib/pages_common.ml:400
msgid "Password:"
msgstr ""

#: src/web/lib/pages_common.ml:511
msgid "Please enter "
msgstr ""

#: src/web/lib/pages_common.ml:440
msgid "Please enter the verification code received by e-mail:"
msgstr ""

#: src/tool/js/tool_js_booth.ml:372
msgid "Please enter your credential:"
msgstr ""

#: src/web/lib/pages_voter.ml:1298
msgid "Please find below your login and password for the election"
msgstr ""

#: src/web/lib/pages_voter.ml:648
msgid "Please log in to confirm your vote."
msgstr ""

#: src/web/lib/pages_common.ml:332
msgid "Please log in:"
msgstr ""

#: src/tool/js/tool_js_fingerprint.ml:58
msgid "Please paste the data for which you want to compute the fingerprint in the text area below:"
msgstr ""

#: src/web/lib/pages_voter.ml:1149
msgid "Please provide the number of grades to see the result of the election according to the Majority Judgment method."
msgstr ""

#: src/web/lib/pages_voter.ml:1203
msgid "Please provide the number of seats to see the result of the election according to the Single Transferable Vote method."
msgstr ""

#: src/web/lib/pages_voter.ml:1054
msgid "Please wait while your ballot is being encrypted…"
msgstr ""

#: src/web/lib/pages_voter.ml:960
msgid "Please wait… "
msgstr ""

#: src/web/lib/pages_common.ml:199 src/web/lib/pages_common.ml:128
msgid "Powered by "
msgstr ""

#: src/tool/js/tool_js_booth.ml:316
msgid "Previous"
msgstr ""

#: src/web/lib/pages_common.ml:206 src/web/lib/pages_common.ml:135
msgid "Privacy policy"
msgstr ""

#: src/web/lib/pages_common.ml:290
msgid "Proceed"
msgstr ""

#: src/web/lib/pages_voter.ml:1243
msgid "Raw events"
msgstr ""

#: src/web/lib/pages_voter.ml:1058
msgid "Restart"
msgstr ""

#: src/web/lib/pages_voter.ml:1421
msgid "Results will be published on the election page"
msgstr ""

#: src/web/lib/pages_voter.ml:1019 src/web/lib/pages_voter.ml:819
#. src/web/lib/pages_voter.ml:817
#. src/web/lib/pages_voter.ml:685
#. src/web/lib/pages_voter.ml:683
msgid "Review and encrypt"
msgstr ""

#: src/web/lib/pages_voter.ml:999
msgid "Save it to check that it is taken into account later."
msgstr ""

#: src/web/lib/pages_voter.ml:312
msgid "See accepted ballots"
msgstr ""

#: src/tool/js/tool_js_booth.ml:80
msgid "Select between %d and %d answer(s)"
msgstr ""

#: src/web/lib/pages_common.ml:229
msgid "Set"
msgstr ""

#: src/web/lib/pages_voter.ml:156
msgid "Several techniques exist to decide which candidate to elect when there is no Condorcet winner."
msgstr ""

#: src/web/lib/pages_voter.ml:228
msgid "Single Transferable Vote"
msgstr ""

#: src/web/lib/pages_voter.ml:1219 src/web/lib/pages_voter.ml:1172
msgid "Single Transferable Vote method"
msgstr ""

#: src/web/lib/pages_voter.ml:326
msgid "Start"
msgstr ""

#: src/web/lib/pages_voter.ml:802 src/web/lib/pages_voter.ml:668
msgid "Step 1"
msgstr ""

#: src/web/lib/pages_voter.ml:1030
msgid "Step 1/6: Input credential"
msgstr ""

#: src/web/lib/pages_voter.ml:811 src/web/lib/pages_voter.ml:677
msgid "Step 2"
msgstr ""

#: src/web/lib/pages_voter.ml:1040
msgid "Step 2/6: Answer to questions"
msgstr ""

#: src/web/lib/pages_voter.ml:820 src/web/lib/pages_voter.ml:686
msgid "Step 3"
msgstr ""

#: src/web/lib/pages_voter.ml:1045
msgid "Step 3/6: Review and encrypt"
msgstr ""

#: src/web/lib/pages_voter.ml:829 src/web/lib/pages_voter.ml:695
msgid "Step 4"
msgstr ""

#: src/web/lib/pages_voter.ml:838 src/web/lib/pages_voter.ml:704
msgid "Step 5"
msgstr ""

#: src/web/lib/pages_common.ml:517 src/web/lib/pages_common.ml:445
msgid "Submit"
msgstr ""

#: src/web/lib/pages_voter.ml:1261
msgid "Such candidates are marked as \"TieWin\" when they are elected and as \"TieLose\" if they have lost."
msgstr ""

#: src/web/lib/pages_voter.ml:866
msgid "Thank you for voting!"
msgstr ""

#: src/web/lib/pages_voter.ml:165
msgid "The Schulze winners are:"
msgstr ""

#: src/web/lib/pages_voter.ml:1270
msgid "The Single Transferable Vote winners are:"
msgstr ""

#: src/web/lib/pages_voter.ml:294 src/web/lib/pages_voter.ml:289
msgid "The election is closed and being tallied."
msgstr ""

#: src/web/lib/pages_voter.ml:279
msgid "The election will close in "
msgstr ""

#: src/web/lib/pages_voter.ml:515
msgid "The fingerprint of the encrypted tally is %s."
msgstr ""

#: src/tool/js/tool_js_fingerprint.ml:47
msgid "The fingerprints differ!"
msgstr ""

#: src/tool/js/tool_js_fingerprint.ml:45
msgid "The fingerprints match!"
msgstr ""

#: src/web/lib/pages_voter.ml:1147
msgid "The number of different grades (Excellent, Very Good, etc.) typically varies from 5 to 7."
msgstr ""

#: src/web/lib/site_voter.ml:330
msgid "The number of grades is invalid."
msgstr ""

#: src/web/lib/site_voter.ml:355
msgid "The number of seats is invalid."
msgstr ""

#: src/web/lib/pages_voter.ml:237
msgid "The raw results can be viewed in the "
msgstr ""

#: src/web/lib/pages_voter.ml:388
msgid "The result of this election is currently not publicly available. It will be in %s."
msgstr ""

#: src/web/lib/site_voter.ml:272
msgid "The result of this election is not available."
msgstr ""

#: src/web/lib/pages_voter.ml:442
msgid "The total weight is %s (min: %s, max: %s)."
msgstr ""

#: src/web/lib/pages_voter.ml:418
msgid "The voter list has %d voter(s) and fingerprint %s."
msgstr ""

#: src/web/lib/pages_voter.ml:1137
msgid "The winner is the candidate with the highest median (or the 2nd highest median if there is a tie, etc.)."
msgstr ""

#: src/web/lib/pages_voter.ml:1255
msgid "There has been at least one tie."
msgstr ""

#: src/web/lib/site_common.ml:52
msgid "This election does not exist. This may happen for elections that have not yet been open or have been deleted."
msgstr ""

#: src/web/lib/pages_voter.ml:299
msgid "This election has been tallied."
msgstr ""

#: src/web/lib/pages_voter.ml:411
msgid "This election is administered by %s."
msgstr ""

#: src/web/lib/pages_voter.ml:304
msgid "This election is archived."
msgstr ""

#: src/web/lib/pages_voter.ml:271
msgid "This election is currently closed."
msgstr ""

#: src/web/lib/pages_voter.ml:431
msgid "This election uses weights!"
msgstr ""

#: src/tool/js/tool_js_booth.ml:387
msgid "This looks like a password... Maybe you looked at the wrong e-mail?"
msgstr ""

#: src/web/lib/site_voter.ml:277
msgid "This question is homomorphic, this method cannot be applied to its result."
msgstr ""

#: src/web/lib/pages_voter.ml:1413
msgid "This vote replaces any previous vote."
msgstr ""

#: src/web/lib/pages_voter.ml:145 src/web/lib/pages_voter.ml:89
msgid "Tie:"
msgstr ""

#: src/web/lib/pages_voter.ml:1356
msgid "To cast a vote, you will also need a password, sent in a separate email."
msgstr ""

#: src/web/lib/pages_voter.ml:1289
msgid "To get more information, please contact:"
msgstr ""

#: src/web/lib/pages_voter.ml:353
msgid "Total weight of accepted ballots:"
msgstr ""

#: src/web/lib/pages_voter.ml:505
msgid "Trustees shuffled the ballots in the following order:"
msgstr ""

#: src/web/lib/pages_common.ml:466
msgid "Use the following code:"
msgstr ""

#: src/web/lib/pages_voter.ml:1362 src/web/lib/pages_voter.ml:1305
#. src/web/lib/pages_common.ml:387
#. src/web/lib/pages_common.ml:342
msgid "Username:"
msgstr ""

#: src/tool/js/tool_js_booth.ml:209
msgid "Value must be an integer between 0 and 255."
msgstr ""

#: src/web/lib/pages_voter.ml:771
msgid "Warning:"
msgstr ""

#: src/tool/js/tool_js_booth.ml:239
msgid "Warning: the system will accept any integer between 0 and 255 but, according to the election rules, invalid ballots (score too high or candidates not properly ranked) will be rejected at the end of the election."
msgstr ""

#: src/web/lib/pages_common.ml:470
msgid "Warning: this code is valid for 15 minutes, and previous codes sent to this address are no longer valid."
msgstr ""

#: src/web/lib/pages_voter.ml:158
msgid "We use here the Schulze method and we refer voters to "
msgstr ""

#: src/web/lib/pages_voter.ml:1190
msgid "When a candidate obtains enough votes to be elected, the votes are transferred to the next candidate in the voter ballot, with a coefficient proportional to the \"surplus\" of votes."
msgstr ""

#: src/web/lib/pages_common.ml:239
msgid "Wish to help with translations?"
msgstr ""

#: src/web/lib/pages_voter.ml:1370 src/web/lib/pages_voter.ml:1315
msgid "You are allowed to vote several times."
msgstr ""

#: src/web/lib/pages_voter.ml:1349
msgid "You are listed as a voter for the election"
msgstr ""

#: src/web/lib/site_common.ml:109
msgid "You are not allowed to access this page!"
msgstr ""

#: src/web/lib/pages_common.ml:424
msgid "You can "
msgstr ""

#: src/web/lib/pages_common.ml:377
msgid "You can also "
msgstr ""

#: src/web/lib/pages_voter.ml:372
msgid "You can also download the "
msgstr ""

#: src/web/lib/pages_voter.ml:862
msgid "You can check its presence in the "
msgstr ""

#: src/web/lib/pages_voter.ml:1417
msgid "You can check its presence in the ballot box, accessible at"
msgstr ""

#: src/web/lib/pages_common.ml:528
msgid "You cannot log in now. Please try later."
msgstr ""

#: src/tool/js/tool_js_booth.ml:127
msgid "You must select at least %d answer(s)"
msgstr ""

#: src/tool/js/tool_js_booth.ml:130
msgid "You must select at most %d answer(s)"
msgstr ""

#: src/web/lib/pages_voter.ml:1354
msgid "You will be asked to enter your credential before entering the voting booth."
msgstr ""

#: src/web/lib/pages_voter.ml:1353
msgid "You will find below your credential."
msgstr ""

#: src/web/lib/pages_voter.ml:879 src/web/lib/pages_voter.ml:731
msgid "Your ballot for "
msgstr ""

#: src/web/lib/pages_voter.ml:990
msgid "Your ballot has been encrypted, "
msgstr ""

#: src/web/lib/site_voter.ml:115
msgid "Your browser seems to block cookies. Please enable them."
msgstr ""

#: src/web/lib/pages_voter.ml:1388
msgid "Your credential for election %s"
msgstr ""

#: src/web/lib/pages_common.ml:465
msgid "Your e-mail address has been used to authenticate with our Belenios server."
msgstr ""

#: src/web/lib/pages_voter.ml:1340
msgid "Your password for election %s"
msgstr ""

#: src/web/lib/pages_voter.ml:1408
msgid "Your smart ballot tracker is"
msgstr ""

#: src/web/lib/pages_voter.ml:995 src/web/lib/pages_voter.ml:857
#. src/web/lib/pages_voter.ml:734
msgid "Your smart ballot tracker is "
msgstr ""

#: src/web/lib/pages_voter.ml:1398
msgid "Your vote for election"
msgstr ""

#: src/web/lib/site_voter.ml:152
msgid "Your vote for election %s"
msgstr ""

#: src/web/lib/pages_voter.ml:773
msgid "Your vote was not recorded!"
msgstr ""

#: src/web/lib/pages_voter.ml:1405 src/web/lib/pages_voter.ml:849
#. src/web/lib/pages_voter.ml:720
msgid "Your weight is %s."
msgstr ""

#: src/web/lib/pages_voter.ml:863
msgid "ballot box"
msgstr ""

#: src/web/lib/pages_voter.ml:67
msgid "ballots"
msgstr ""

#: src/web/lib/pages_voter.ml:991
msgid "but not cast yet"
msgstr ""

#: src/web/lib/pages_common.ml:380
msgid "change your password"
msgstr ""

#: src/web/lib/pages_common.ml:378
msgid "create an account"
msgstr ""

#: src/web/lib/pages_voter.ml:1402
msgid "has been recorded."
msgstr ""

#: src/tool/js/tool_js_booth.ml:370 src/web/lib/pages_voter.ml:1193
#. src/web/lib/pages_voter.ml:1140
msgid "here"
msgstr ""

#: src/web/lib/pages_voter.ml:1196
msgid "our code of STV"
msgstr ""

#: src/web/lib/pages_voter.ml:57
msgid "parameters"
msgstr ""

#: src/web/lib/pages_voter.ml:400
msgid "personal data policy"
msgstr ""

#: src/web/lib/pages_voter.ml:63
msgid "public credentials"
msgstr ""

#: src/web/lib/pages_voter.ml:374
msgid "result with cryptographic proofs"
msgstr ""

#: src/web/lib/web_common.ml:76
msgid "some proofs failed verification"
msgstr ""

#: src/web/lib/pages_voter.ml:777
msgid "start from the beginning"
msgstr ""

#: src/web/lib/pages_voter.ml:159
msgid "the Wikipedia page"
msgstr ""

#: src/web/lib/web_common.ml:72
msgid "the election is closed"
msgstr ""

#: src/web/lib/pages_voter.ml:60
msgid "trustees"
msgstr ""

#: src/web/lib/pages_common.ml:425
msgid "try to log in again"
msgstr ""

#: src/web/lib/web_common.ml:78
msgid "you are not allowed to revote"
msgstr ""

#: src/web/lib/web_common.ml:73
msgid "you are not allowed to vote"
msgstr ""

#: src/web/lib/web_common.ml:80
msgid "you are not allowed to vote with this credential"
msgstr ""

#: src/web/lib/web_common.ml:74
msgid "your ballot has a syntax error (%s)"
msgstr ""

#: src/web/lib/web_common.ml:75
msgid "your ballot is not in canonical form"
msgstr ""

#: src/web/lib/web_common.ml:81
msgid "your credential has a bad weight"
msgstr ""

#: src/web/lib/web_common.ml:79
msgid "your credential has already been used"
msgstr ""

#: src/web/lib/web_common.ml:77
msgid "your credential is invalid"
msgstr ""

